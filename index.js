import { http } from '@tauri-apps/api';

import buildURL from 'axios/lib/helpers/buildURL';
import buildFullPath from 'axios/lib/core/buildFullPath';
import {
    isFormData,
    isPlainObject,
    isArrayBuffer,
    isString,
    isBlob
} from 'axios/lib/utils';
import AxiosError from 'axios/lib/core/AxiosError';
import { transitional } from 'axios/lib/defaults';
import settle from 'axios/lib/core/settle';

import {
    ReasonPhrases,
    StatusCodes
} from 'http-status-codes';

const
    clients = {},
    getClient = async options => {
        let client = clients[JSON.stringify(options)];
        if(!client) client = await http.getClient(options);
        return client;
    };

export default config => new Promise((resolve, reject) => {
    (async () => {
        const timeout = config.timeout > 0 ? Math.round(config.timeout / 1000) * 1000 : undefined;
        if(timeout && timeout !== config.timeout)
            console.warn(`Timeout of ${config.timeout}ms rounded to ${timeout}ms`);
        try {
            const
                request = (await getClient({
                    maxRedirections: config.maxRedirects
                })).request({
                    url: buildURL(buildFullPath(config.baseURL, config.url), config.params, config.paramsSerializer),
                    method: config.method,
                    headers: {
                        ...config.headers,
                        ...config.auth && { 'Authorization': `Basic ${btoa(`${config.auth.username || ''}:${unescape(encodeURIComponent(config.auth.password || ''))}`)}` }
                    },
                    body: config.data && {
                        type: isFormData(config.data)
                            ? 'Form'
                            : isPlainObject(config.data)
                                ? 'Json'
                                : isString(config.data)
                                    ? 'Text'
                                    : (isArrayBuffer(config.data) || isBlob(config.data))
                                        ? 'Bytes'
                                        : undefined,
                        payload: isBlob(config.data)
                                ? await config.data.arrayBuffer()
                                : config.data
                    },
                    timeout: timeout / 1000,
                    responseType: {
                        'arraybuffer': http.ResponseType.Binary,
                        'blob': http.ResponseType.Binary,
                        'json': http.ResponseType.JSON,
                        'text': http.ResponseType.Text
                    }[config.responseType]
                }),
                {
                    status,
                    headers,
                    data
                } = await request;
            settle(resolve, reject, {
                data: config.responseType === 'arraybuffer'
                    ? new Uint8Array(data).buffer
                    : config.responseType === 'blob'
                        ? new Blob([new Uint8Array(data)], { type: 'application/octet-stream' })
                        : data,
                status,
                statusText: ReasonPhrases[StatusCodes[status]],
                headers,
                config,
                request
            });
        }
        catch(error){
            if([
                'failed to execute API: Network Error: Io Error: timed out',
                'failed to execute API: Network Error: Io Error: connection timed out'
            ].includes(error)){
                throw new AxiosError(
                    `timeout of ${timeout}ms exceeded`,
                    config,
                    (config.transitional || transitional).clarifyTimeoutError ? 'ETIMEDOUT' : 'ECONNABORTED'
                );
            }
            else throw error;
        }
    })().catch(reject);
});