# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0](https://git.kaki87.net/KaKi87/axios-tauri-adapter/compare/0.2.0...0.3.0) (2023-07-11)

### Upgraded

- Tauri : `tauri` (crates) v1.4.1, `@tauri-apps/cli` (npm) v1.4.0, `@tauri-apps/api` (npm) v1.4.0

## [0.2.0](https://git.kaki87.net/KaKi87/axios-tauri-adapter/compare/0.1.1...0.2.0) (2022-10-25)

### Added

- shields.io badges in [`README.md`](./README.md) (d4402d5)
- `config.maxRedirects` support (4937cd8)
- `response.request` property (355e8d0)

### Fixed

- `config.responseType` = `blob` support (61c0a26)

### Upgraded

- Tauri : `tauri` (crates) v1.1.1, `@tauri-apps/cli` (npm) v1.1.1, `@tauri-apps/api` (npm) v1.1.0

## [0.1.1](https://git.kaki87.net/KaKi87/axios-tauri-adapter/compare/0.1.0...0.1.1) (2021-11-04)

### Added

- Link to unit testing repository in [`README.md`](./README.md) (c56efc4)
- Link to repository in [`package.json`](./package.json) (dae1c00)
- Changelog

### Fixed

- Worked around [*"Failed to parse response"* error](https://github.com/tauri-apps/tauri/issues/2831) on unset response data (70d8039)

## 0.1.0 (2021-10-22)

Initial release