# axios-tauri-adapter

[![](https://shields.kaki87.net/npm/v/axios-tauri-adapter)](https://www.npmjs.com/package/axios-tauri-adapter)
![](https://shields.kaki87.net/npm/dw/axios-tauri-adapter)
![](https://shields.kaki87.net/npm/l/axios-tauri-adapter)

An [Axios](https://axios-http.com/) adapter for the [`http`](https://tauri.studio/en/docs/api/js/modules/http) module of the [Tauri](https://tauri.studio/) framework.

## Installation

```
yarn add axios axios-tauri-adapter
```

## Usage

```js
import axios from 'axios';
import axiosTauriAdapter from 'axios-tauri-adapter';
// globally :
axios.defaults.adapter = axiosTauriAdapter;
// or locally :
const client = axios.create({ adapter: axiosTauriAdapter });
```

## Compatibility

| Adapter version | Axios version     | Tauri version            |
|-----------------|-------------------|--------------------------|
| `0.1.x`-`0.2.x` | `0.23.x`-`0.27.x` | `1.0.0-beta.8` & greater |
| `0.3.x`         | `0.23.x`-`0.27.x` | `1.0.0-rc.0` & greater   |

## [Unit testing](https://git.kaki87.net/KaKi87/axios-tauri-adapter-test)

## [Changelog](./CHANGELOG.md)